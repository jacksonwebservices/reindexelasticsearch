# Reindex Elasticsearch
The reindex elasticsearch script is a simple tool that is responsible for reindexing documents as an aging scheme for daily indices.  
The way the code is written, if an error happens with either of the two reindex operations, the program exists after storing the error
info before performing the delete operation, thus negating the possibility of data loss if delete happened before a successful reindex.  

* Reindex.py uses an Elasticsearch instance to run reindex operations and subsequent delete operations.
* the scripts directory holds the two reindex operation script bodies for error and traffic logs.
* reports.log holds the report information of each program run for history and debugging purposes.
