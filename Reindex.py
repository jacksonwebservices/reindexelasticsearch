#################################################
# Author: Christopher Hunt
# File: Reindex.py
# Purpose: The Reindex class implements the elasticsearch python library to perform a simple
#     reindexing operation on aging timebase indexes.
# Dependencies:
#     -python3, elasticsearch, datetime, json
#################################################
from elasticsearch import Elasticsearch
import datetime
import calendar
import json

class Reindex:

    def __init__(self):
        self.es = Elasticsearch()

    def get_week_dates(self):
        """
        :description: get_weed_dates uses python datetime to get a list of dates for last week.
            The functionality is designed to always return dates for previous week, irregardless
            of when the function is called.
        :param: none
        :return: lastWeekDates list of dates
        :see: datetime
        """
        today = datetime.date.today()
        weekday = today.weekday()+1	    # python starts week on Monday, Elasticsearch starts week on Sunday
        startDelta = datetime.timedelta(days=weekday,  weeks=1)
        startOfWeek = today - startDelta
        
        lastWeekDates = []
        for day in range(7):
            lastWeekDates.append((startOfWeek + datetime.timedelta(days=day)).strftime("%d.%m.%Y"))
        return lastWeekDates

    def get_month_dates(self):
        """
        :description: get_month_dates uses python datetime to get a list of dates for last week.
            The functionality is designed to always return dates for previous month, irregardless
            of when the function is called.
        :param: none
        :return: lastWeekDates list of dates
        :see: datetime
        """
        first = datetime.date.today().replace(day=1)
        lastMonthDate = first - datetime.timedelta(days=1)
        lastMonthYear = lastMonthDate.strftime('%m.%Y')
        year = lastMonthDate.strftime('%Y')
        numDays = calendar.monthrange(int(year), int(lastMonthDate.strftime('%m')))[1]
        lastMonthDates = []
        for i in range(numDays):
            lastMonthDates.append("%02d.%s" % (i+1, lastMonthYear))  # format day with a leading 0 for single digits
        return lastMonthDates


    def reindex(self, filePath):
        """
        :descrption: reindex takes the path to the reindex operation body sends it to ES.
        :param: filePath str
        :return: report dict
        :see: none
        """
        report = {}
        with open(filePath, "r") as f:
            request = f.read()
        status = self.es.reindex(body=request)
        report["Reindex Job: " + str(filePath)] = status
        return report
        
    def delete(self, indexPrefix):
        """
        :description: delete uses date list returned from get_dates and deletes 7 daily
            indices for the previous week where index name = indexPrefix + date for each 
            date in the date list.
        :param: indexPrefix
        :return: report dict
        :see: get_dates() method
        """
        report = {}
        lastMonthsDates = self.get_month_dates()

        for day in lastMonthsDates:
            indexName = indexPrefix + str(day)

            # ensure all indecies existing are deleted, even if some do not exist.
            try:
                status = self.es.indices.delete(index=indexName)
                report["Delete Day-- " + str(day)] = {str(indexName): str(status)}
            except Exception as status:
                report["Delete Day ERROR-- " +  str(day)] = {str(indexName): str(status)}

        return report

    def store_report(self, filePath, report):
        """
        :TODO: add delete_report as param and un-comment report write
        :description: stores report json objects after reindex and delete operations are called.
        :param: filePath str, report dict
        :return: none
        :see: json
        """
        with open(filePath, "a") as f:
            f.write(datetime.date.today().strftime("%D.%m.%Y."))
            if isinstance(report, dict):
                f.write(json.dumps(report))
            else:
                f.write(report)

            f.write("\n \n")

    def run(self):
        """
        :description: calls both reindex operations and delete operation, ensuring each is successful before moving
            to the next. If any call fails, the report is returned for that called. This ensures reindexing happens
            before delete, removing the possibility of data loss if delete happens before a successful reindex.
        :param: none
        :return: report (single if error, all if full success)
        :see: none
        """
        reports = []
        # ensure reindex operation is successful before moving on
        try:
            errorLogsReport = self.reindex("scripts/reindex_errors.json")
        except Exception as excpt:
            return "ERROR in reindex_errors-- " + str(repr(excpt))

        # ensure reindex operation is successful before calling delete
        try:
            trafficLogsReport = self.reindex("scripts/reindex_traffic.json")
        except Exception as excpt:
            return "ERROR in reindex_traffic-- " + str(repr(excpt))

        deleteReport = self.delete("test-index-")

        reports.extend([errorLogsReport, trafficLogsReport, deleteReport])
        fullReport = {}
        
        # generate report, accounting for error cases where report types != dicts
        for single in reports:
            if isinstance(single, dict):
                fullReport.update(single)
            else:
                fullReport[single] = "ERROR"

        return fullReport

if __name__ == "__main__": 
    reindex = Reindex()
    full_report = reindex.run()
    reindex.store_report("LoggingProdReports.log", full_report)
